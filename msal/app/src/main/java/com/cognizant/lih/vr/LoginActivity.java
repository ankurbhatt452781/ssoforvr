package com.cognizant.lih.vr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;
import com.microsoft.identity.client.*;
import com.microsoft.identity.client.exception.*;

public class LoginActivity extends AppCompatActivity {
    /* Azure AD v2 Configs */
    final static String[] SCOPES = {"https://graph.microsoft.com/User.Read"};
    /* Azure AD Variables */
    private PublicClientApplication sampleApp;
    private IAuthenticationResult authResult;
    private static final String TAG = LoginActivity.class.getSimpleName();

    private TextView tokenTxt;
    private Button loginBtn;
    private Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tokenTxt = findViewById(R.id.access_token_txt);
        loginBtn = findViewById(R.id.login_btn);
        logoutBtn = findViewById(R.id.logout_btn);

        /* Configure your sample app and save state for this activity */
        sampleApp = new PublicClientApplication(
                this.getApplicationContext(),
                R.raw.auth_config);


        /* Attempt to get a user and acquireTokenSilent
         * If this fails we do an interactive request
         */
        sampleApp.getAccounts(new PublicClientApplication.AccountsLoadedCallback() {
            @Override
            public void onAccountsLoaded(final List<IAccount> accounts) {

                if (!accounts.isEmpty()) {
                    /* This sample doesn't support multi-account scenarios, use the first account */
                    sampleApp.acquireTokenSilentAsync(SCOPES, accounts.get(0), getAuthSilentCallback());
                } else {
                    /* No accounts or >1 account */
                }
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                sampleApp.acquireToken(getActivity(), SCOPES, getAuthInteractiveCallback());
            }
        });
          logoutBtn.setOnClickListener(new View.OnClickListener(){
               public void onClick(View v) {
                   /* Attempt to get a user and acquireTokenSilent
                    * If this fails we do an interactive request
                    */
                   sampleApp.getAccounts(new PublicClientApplication.AccountsLoadedCallback() {
                       @Override
                       public void onAccountsLoaded(final List<IAccount> accounts) {

                           if (accounts.isEmpty()) {
                               /* No accounts to remove */

                           } else {
                               for (final IAccount account : accounts) {
                                   sampleApp.removeAccount(
                                           account,
                                           new PublicClientApplication.AccountsRemovedCallback() {
                                               @Override
                                               public void onAccountsRemoved(Boolean isSuccess) {
                                                   if (isSuccess) {
                                                       /* successfully removed account */
                                                   } else {
                                                       /* failed to remove account */
                                                   }
                                               }
                                           });
                               }
                           }


                       }
                   });
                   tokenTxt.setVisibility(View.GONE);
               }
        });
    }
    //
    // App callbacks for MSAL
    // ======================
    // getActivity() - returns activity so we can acquireToken within a callback
    // getAuthSilentCallback() - callback defined to handle acquireTokenSilent() case
    // getAuthInteractiveCallback() - callback defined to handle acquireToken() case
    //

    public Activity getActivity() {
        return this;
    }

    /* Callback used in for silent acquireToken calls.
     * Looks if tokens are in the cache (refreshes if necessary and if we don't forceRefresh)
     * else errors that we need to do an interactive request.
     */
    private AuthenticationCallback getAuthSilentCallback() {
        return new AuthenticationCallback() {

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, call graph now */
                Log.d(TAG, "Successfully authenticated");

                /* Store the authResult */
                authResult = authenticationResult;
                tokenTxt.setVisibility(View.VISIBLE);
                tokenTxt.setText(authResult.getAccessToken().toString());
                loginBtn.setVisibility(View.GONE);

            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());

                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                } else if (exception instanceof MsalUiRequiredException) {
                    /* Tokens expired or no session, retry with interactive */
                }
            }

            @Override
            public void onCancel() {
                /* User cancelled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }

    /* Callback used for interactive request.  If succeeds we use the access
     * token to call the Microsoft Graph. Does not check cache
     */
    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, call graph now */
                Log.d(TAG, "Successfully authenticated");
                Log.d(TAG, "ID Token: " + authenticationResult.getIdToken());

                /* Store the auth result */
                authResult = authenticationResult;
                tokenTxt.setVisibility(View.VISIBLE);
                tokenTxt.setText(authResult.getAccessToken().toString());
                loginBtn.setVisibility(View.GONE);
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());

                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                }
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }
}
